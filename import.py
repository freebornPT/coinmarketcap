import json, requests

url = 'https://api.coinmarketcap.com/v1/ticker/'

resp = requests.get(url=url)
data = json.loads(resp.text)

with open("log\output.txt", "w") as output:
  output.write(str(data))
